package se.sensera.banking.Impl;

import lombok.AllArgsConstructor;
import lombok.Data;
import se.sensera.banking.Account;
import se.sensera.banking.User;

import java.util.stream.Stream;

@Data
@AllArgsConstructor
public class AccountImpl implements Account {
    String id;
    User owner;
    String name;
    boolean active;
    Stream<User> users;

    @Override
    public void addUser(User user) {

    }

    @Override
    public void removeUser(User user) {

    }
}
