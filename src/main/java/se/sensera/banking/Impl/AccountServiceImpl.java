package se.sensera.banking.Impl;

import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;


import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class AccountServiceImpl implements AccountService {
    private UsersRepository usersRepository;
    private AccountsRepository accountsRepository;

    public AccountServiceImpl(UsersRepository usersRepository, AccountsRepository accountsRepository) {
        this.usersRepository = usersRepository;
        this.accountsRepository = accountsRepository;
    }

    @Override
    public Account createAccount(String userId, String accountName) throws UseException {
        User user = getUser(userId);

        verifyAccountOwner(userId);

        AccountImpl account = new AccountImpl(UUID.randomUUID().toString(), user, accountName, true, Stream.of());
        return accountsRepository.save(account);
    }

    private void verifyAccountOwner(String userId) throws UseException {
        if (accountsRepository.all()
                .anyMatch(acc -> acc.getOwner().getId().equals(userId)))
            throw new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
    }

    private User getUser(String userId) throws UseException {
        return usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
    }

    @Override
    public Account changeAccount(String userId, String accountId, Consumer<ChangeAccount> changeAccountConsumer) throws UseException {
        Account accountFromId = accountsRepository.getEntityById(accountId).get();

        // Account doesn't belong to user.
        if (!accountFromId.getOwner().getId().equals(userId)) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        //name change failed because account inactive
        if (!accountFromId.isActive()) {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);
        }

        ChangeAccountConsumer changeAccountConsumer1 = new ChangeAccountConsumer(accountFromId, name -> accountsRepository.all()
                .filter(accFromRepo -> accFromRepo.getOwner().getId().equals(userId))
                .anyMatch(accFromRepo -> accFromRepo.getName().equals(name)));
        changeAccountConsumer.accept(changeAccountConsumer1);

        if (!changeAccountConsumer1.save){
            return accountFromId;
        }

        return accountsRepository.save(accountFromId);
    }

    @Override
    public Account addUserToAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException {
        return null;
    }

    @Override
    public Account removeUserFromAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException {
        return null;
    }

    @Override
    public Account inactivateAccount(String userId, String accountId) throws UseException {
        //Handles account not found
        Account accountFromId = accountsRepository.getEntityById(accountId).orElseThrow(() ->
                new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_FOUND)
        );

        User user = usersRepository.getEntityById(userId).orElseThrow(() ->
                new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND)
        );

        if (!accountFromId.getOwner().getId().equals(userId))
            throw new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_OWNER);

        if (!accountFromId.isActive())
            throw new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);

        if (!user.isActive())
            throw new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);


        accountFromId.setActive(false);
        return accountsRepository.save(accountFromId);
    }

    @Override
    public Stream<Account> findAccounts(String searchValue, String userId, Integer pageNumber, Integer pageSize, SortOrder sortOrder) throws UseException {
        Stream<Account> all = accountsRepository.all();
        if (searchValue != null && !searchValue.equals("")) {
            all = all.filter(a -> a.getName().contains(searchValue));
        }

        //pagesize 3
        if (sortOrder == SortOrder.AccountName)
            all = all.sorted(Comparator.comparing(Account::getName));

        Predicate<Account> ownerOrAssociated = account -> {
            if (account.getOwner().getId().equals(userId))
                return true;

            if (account.getUsers().anyMatch(user -> user.getId().equals(userId)))
                return true;
            return false;
        };

        if (userId != null && !userId.equals("")) {
            all = all.filter(ownerOrAssociated);
        }

        return ListUtils.applyPage(all, pageNumber, pageSize);
    }

    private static class ChangeAccountConsumer implements ChangeAccount {
        Account account;
        boolean save = false;
        Predicate<String> testNameUnique;

        public ChangeAccountConsumer(Account account, Predicate<String> testNameUnique) {
            this.account = account;
            this.testNameUnique = testNameUnique;
        }

        @Override
        public void setName(String name) throws UseException {
            if (account.getName().equals(name)) {
                return;
            }

            if (testNameUnique.test(name)) {
                throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
            }

            //Sets the given name to the account
            save = true;
            account.setName(name);
        }
    }
}

