package se.sensera.banking.Impl;

import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class TransactionServiceImpl implements TransactionService {
    private UsersRepository usersRepository;
    private AccountsRepository accountsRepository;
    private TransactionsRepository transactionsRepository;

    public TransactionServiceImpl(UsersRepository usersRepository, AccountsRepository accountsRepository, TransactionsRepository transactionsRepository) {
        this.usersRepository = usersRepository;
        this.accountsRepository = accountsRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public Transaction createTransaction(String created, String userId, String accountId, double amount) throws UseException {
        Transaction transaction = null;
        AtomicBoolean save = new AtomicBoolean(true);

        try {

            int fail = testsPassed(created, userId, accountId, amount, usersRepository, accountsRepository, transactionsRepository);
            switch (fail) {
                case 1:
                    save.set(false);
                    throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ALLOWED);
                case 2:
                    save.set(false);
                    throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FUNDED);
            }

            transaction = new TransactionImpl(UUID.randomUUID().toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(created),
                    usersRepository.getEntityById(userId).get(), accountsRepository.getEntityById(accountId).get(),
                    amount);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(!save.get()) return transaction;
        return transactionsRepository.save(transaction);
    }

    public static int testsPassed(String created, String userId, String accountId, double amount, UsersRepository usersRepository, AccountsRepository accountsRepository, TransactionsRepository transactionsRepository) throws UseException{
        //return false if user not owner of account
        Account account = accountsRepository.getEntityById(accountId).get();
        if (!account.getOwner().getId().equals(userId)){
            return 1;
        }
        // Not enough funds at account
        if (amount < 0){
            return 2;
        }

        return 0;
    }

    @Override
    public double sum(String created, String userId, String accountId) throws UseException {
        return 0;
    }

    @Override
    public void addMonitor(Consumer<Transaction> monitor) {

    }
}
