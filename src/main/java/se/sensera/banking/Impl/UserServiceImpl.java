package se.sensera.banking.Impl;

import se.sensera.banking.User;
import se.sensera.banking.UserService;
import se.sensera.banking.UsersRepository;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static se.sensera.banking.exceptions.Activity.CREATE_USER;
import static se.sensera.banking.exceptions.UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE;

public class UserServiceImpl implements UserService {

    private UsersRepository usersRepository;

    public UserServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public User createUser(String name, String personalIdentificationNumber) throws UseException {
        long IdIsUnique = usersRepository.all()
                .filter(u -> u.getPersonalIdentificationNumber().equals(personalIdentificationNumber))
                .count();

        if (IdIsUnique > 0) {
            throw new UseException(CREATE_USER, USER_PERSONAL_ID_NOT_UNIQUE, "Tjo fadderittan");
        } else {
            UserImpl user = new UserImpl(UUID.randomUUID().toString(), name, personalIdentificationNumber, true);
            return usersRepository.save(user);
        }
    }

    @Override
    public User changeUser(String userId, Consumer<ChangeUser> changeUser) throws UseException {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        AtomicBoolean save = new AtomicBoolean(false);
        changeUser.accept(new ChangeUser() {
            @Override
            public void setName(String name) {
                user.setName(name);
                save.set(true);
            }

            @Override
            public void setPersonalIdentificationNumber(String personalIdentificationNumber) throws UseException {
                boolean notUnique = usersRepository.all()
                        .anyMatch(u -> u.getPersonalIdentificationNumber().equals(personalIdentificationNumber));
                if(notUnique)
                    throw new UseException(Activity.UPDATE_USER, USER_PERSONAL_ID_NOT_UNIQUE);
                user.setPersonalIdentificationNumber(personalIdentificationNumber);
                save.set(true);
            }
        });
        if(save.get() == false) return user;
        return usersRepository.save(user);
    }

    @Override
    public User inactivateUser(String userId) throws UseException {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        user.setActive(false);
        return usersRepository.save(user);
    }

    @Override
    public Optional<User> getUser(String userId) {
        return usersRepository.getEntityById(userId);
    }

    @Override
    public Stream<User> find(String searchString, Integer pageNumber, Integer pageSize, SortOrder sortOrder) {
        Stream<User> all = usersRepository.all();
        if (searchString != null && !searchString.isEmpty()){
            all = all
                    .filter(u -> u.getName().toLowerCase().contains(searchString.toLowerCase()))
                    .filter(User::isActive);
        }

        //Sort order
        switch (sortOrder) {
            case Name:
                all = all.sorted(Comparator.comparing(User::getName));
                break;
            case PersonalId:
                all = all.sorted(Comparator.comparing(User::getPersonalIdentificationNumber));
                break;
        }

        all = all.filter(user -> user.isActive());

        return ListUtils.applyPage(all, pageNumber, pageSize);
    }
}
